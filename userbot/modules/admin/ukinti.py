""" @ukinti_bot
Available Commands:
.unbanall
.kick option
Available Options: d, y, m, w, o, q, r, p 
p - reserved for channel
e - usercount
y - userstatusempty
m - userstatuslastmonth
w - userstatuslastweek
o - userstatusoffline
q - userstatusonline
r - userstatusrecently
b - bot
d - deleted account"""
from asyncio import sleep
from os import remove

from telethon.errors import (BadRequestError, ChatAdminRequiredError,
                             ImageProcessFailedError, PhotoCropSizeSmallError,
                             UserAdminInvalidError)
from telethon.errors.rpcerrorlist import (UserIdInvalidError,
                                          MessageTooLongError)
from telethon.tl.functions.channels import (EditAdminRequest,
                                            EditBannedRequest,
                                            EditPhotoRequest)
from telethon.tl.functions.messages import UpdatePinnedMessageRequest
from telethon.tl.types import (PeerChat, ChannelParticipantsAdmins, ChatAdminRights, ChatBannedRights, 
                               MessageEntityMentionName, MessageMediaPhoto, ChannelParticipantsBots, 
                               UserStatusEmpty, UserStatusLastMonth, UserStatusLastWeek, UserStatusOffline, 
                               UserStatusOnline, UserStatusRecently, ChannelParticipantsKicked)
from ..help import add_help_item
from userbot import BOTLOG, BOTLOG_CHATID, bot
from userbot.events import register

async def _(event):
    if event.fwd_from:
        return
    input_str = event.pattern_match.group(1)
    if input_str:
        logger.info("TODO: Not yet Implemented")
    else:
        if event.is_private:
            return False
        await event.edit("Searching Participant Lists.")
        p = 0
        async for i in event.iter_participants(event.chat_id, filter=ChannelParticipantsKicked, aggressive=True):
            rights = ChatBannedRights(
                until_date=0,
                view_messages=False
            )
            try:
                await event(functions.channels.EditBannedRequest(event.chat_id, i, rights))
            except FloodWaitError as ex:
                logger.warn("sleeping for {} seconds".format(ex.seconds))
                sleep(ex.seconds)
            except Exception as ex:
                await event.edit(str(ex))
            else:
                p += 1
        await event.edit("{}: {} unbanned".format(event.chat_id, p))


@register(outgoing=True, pattern="^\.ukick$")
async def rm_deletedacc(event):
    if event.fwd_from:
        return
    if event.is_private:
        return False
    chat = await event.get_chat()
    admin = chat.admin_rights
    creator = chat.creator
    input_str = admin and creator
    if not admin and not creator:
        return await show.edit("`I am not an admin here!`")
    await event.edit("Searching Participant Lists.")
    async for i in event.client.iter_participants(event.chat_id):
        #
        # Note that it's "reversed". You must set to ``True`` the permissions
        # you want to REMOVE, and leave as ``None`` those you want to KEEP.
        rights = ChatBannedRights(
            until_date=None,
            view_messages=True
        )
        if isinstance(UserStatusLastMonth, UserStatusLastWeek):
          e = ban_user(event.chat_id, i, rights)
          e.append(str(e))
async def ban_user(chat_id, i, rights):
    try:
        await event(functions.channels.EditBannedRequest(chat_id, i, rights))
        return True, None
    except Exception as exc:
        return False, str(exc)
