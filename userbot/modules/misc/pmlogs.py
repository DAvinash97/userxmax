#This module was created by @spechide for Uniborg
"""Log PMs
this will now log chat msgs to your botlog chat id.
Thanks to @heyworld for a small correction"""

import asyncio
import os
from telethon.tl.functions.photos import GetUserPhotosRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import MessageEntityMentionName
from telethon.utils import get_input_location
from telethon import events
from telethon.tl import functions, types
from userbot import PM_LOGGER, CMD_HELP, bot, TEMP_DOWNLOAD_DIRECTORY, LOGS
from userbot.events import register

@register(incoming=True)
async def monito_p_m_s(event):
    sender = await event.get_sender()
    if event.is_private and not (await event.get_sender()).bot:
        chat = await event.get_chat()
        if chat.id:
            try:
                e = await event.client.get_entity(int(PM_LOGGER))
                fwd_message = await event.client.forward_messages(
                    e,
                    event.message,
                    silent=True
                )
            except Exception as e:
                LOGS.warn(str(e))
