#This module was created by @spechide for Uniborg
"""Log PMs
this will now log chat msgs to your botlog chat id.
if you don't want chat logs than use `.nolog` , for opposite use `.log`. Default is .log enabled.
enjoy this now.
Thanks to @heyworld for a small correction"""

import asyncio
import os
from telethon.tl.functions.photos import GetUserPhotosRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import MessageEntityMentionName
from telethon.utils import get_input_location
from telethon import events
from telethon.tl import functions, types
from userbot import PM_LOGGER_ID, CMD_HELP, bot, TEMP_DOWNLOAD_DIRECTORY
from userbot.events import register

@register(incoming=True)
async def monito_p_m_s(event):
    sender = await event.get_sender()
    if event.is_private and not (await event.get_sender()).bot:
        chat = await event.get_chat()
        e = await event.client.get_entity(int(PM_LOGGER_ID))
        fwd_message = await event.client.forward_messages(
                    e,
                    event.message,
                    silent=True)
